"""
    flaskext.babel
    ~~~~~~~~~~~~~~

    Implements i18n/l10n support for Flask applications based on Babel.

    :copyright: (c) 2013 by Armin Ronacher, Daniel Neuhäuser.
    :license: BSD, see LICENSE for more details.
"""
import os

import flask
from babel import support, Locale
from contextlib import contextmanager


class Babel(object):
    """Central controller class that can be used to configure how
    Flask-Babel behaves.  Each application that wants to use Flask-Babel
    has to create, or run :meth:`init_app` on, an instance of this class
    after the configuration was initialized.
    """

    def __init__(self, app=None, default_locale='en', default_domain='messages', 
                date_formats=None, configure_jinja=True):
        self._default_locale = default_locale
        self._default_domain = default_domain
        self._configure_jinja = configure_jinja
        self.app = app
        self.locale_selector_func = None

        if app is not None:
            self.init_app(app)

    def init_app(self, app):
        """Set up this instance for use with *app*, if no app was passed to
        the constructor.
        """
        self.app = app
        app.babel_instance = self
        if not hasattr(app, 'extensions'):
            app.extensions = {}
        app.extensions['babel'] = self

        app.config.setdefault('BABEL_DEFAULT_LOCALE', self._default_locale)
        app.config.setdefault('BABEL_DOMAIN', self._default_domain)

        #: a mapping of Babel datetime format strings that can be modified
        #: to change the defaults.  If you invoke :func:`format_datetime`
        #: and do not provide any format string Flask-Babel will do the
        #: following things:
        #:
        #: 1.   look up ``date_formats['datetime']``.  By default ``'medium'``
        #:      is returned to enforce medium length datetime formats.
        #: 2.   ``date_formats['datetime.medium'] (if ``'medium'`` was
        #:      returned in step one) is looked up.  If the return value
        #:      is anything but `None` this is used as new format string.
        #:      otherwise the default for that language is used.

        if self._configure_jinja: # mods for the jinja _ implementation
            app.jinja_env.add_extension('jinja2.ext.i18n')
            app.jinja_env.install_gettext_callables(
                lambda x: get_translations().ugettext(x),
                lambda s, p, n: get_translations().ungettext(s, p, n),
                newstyle=True
            )

    def localeselector(self, f):
        """Registers a callback function for locale selection.  The default
        behaves as if a function was registered that returns `None` all the
        time.  If `None` is returned, the locale falls back to the one from
        the configuration.

        This has to return the locale as string (eg: ``'de_AT'``, ``'en_US'``)
        """
        self.locale_selector_func = f
        return f

    def list_translations(self):
        """Returns a list of all the locales translations exist for.  The
        list returned will be filled with actual locale objects and not just
        strings.

        .. versionadded:: 0.6
        """
        result = []

        for dirname in self.translation_directories:
            if not os.path.isdir(dirname):
                continue

            for folder in os.listdir(dirname):
                locale_dir = os.path.join(dirname, folder, 'LC_MESSAGES')
                if not os.path.isdir(locale_dir):
                    continue

                if filter(lambda x: x.endswith('.mo'), os.listdir(locale_dir)):
                    result.append(Locale.parse(folder))

        # If not other translations are found, add the default locale.
        if not result:
            result.append(Locale.parse(self._default_locale))

        return result

    @property
    def default_locale(self):
        """The default locale from the configuration as instance of a
        `babel.Locale` object.
        """
        return Locale.parse(self.app.config['BABEL_DEFAULT_LOCALE'])

    @property
    def domain(self):
        """The message domain for the translations as a string.
        """
        return self.app.config['BABEL_DOMAIN']

    @flask.helpers.locked_cached_property
    def domain_instance(self):
        """The message domain for the translations.
        """
        return Domain(domain=self.app.config['BABEL_DOMAIN'])

    @property
    def translation_directories(self):
        directories = self.app.config.get(
            'BABEL_TRANSLATION_DIRECTORIES',
            'translations'
        ).split(';')

        for path in directories:
            if os.path.isabs(path):
                yield path
            else:
                yield os.path.join(self.app.root_path, path)


def get_translations():
    """Returns the correct gettext translations that should be used for
    this request.  This will never fail and return a dummy translation
    object if used outside of the request or if a translation cannot be
    found.
    """
    return get_domain().get_translations()


def get_locale():
    """Returns the locale that should be used for this request as
    `babel.Locale` object.  This returns `None` if used outside of
    a request.
    """
    ctx = _get_current_context()
    if ctx is None:
        return None
    locale = getattr(ctx, 'babel_locale', None)
    if locale is None:
        babel = flask.current_app.extensions['babel']
        if babel.locale_selector_func is None:
            locale = babel.default_locale
        else:
            rv = babel.locale_selector_func()
            if rv is None:
                locale = babel.default_locale
            else:
                locale = Locale.parse(rv)
        ctx.babel_locale = locale
    return locale


@contextmanager
def force_locale(locale):
    """Temporarily overrides the currently selected locale.

    Sometimes it is useful to switch the current locale to different one, do
    some tasks and then revert back to the original one. For example, if the
    user uses German on the web site, but you want to send them an email in
    English, you can use this function as a context manager::

        with force_locale('en_US'):
            send_email(gettext('Hello!'), ...)

    :param locale: The locale to temporary switch to (ex: 'en_US').
    """
    ctx = _get_current_context()
    if ctx is None:
        yield
        return

    orig_attrs = {}
    for key in ('babel_translations', 'babel_locale'):
        orig_attrs[key] = getattr(ctx, key, None)

    try:
        ctx.babel_locale = Locale.parse(locale)
        ctx.forced_babel_locale = ctx.babel_locale
        ctx.babel_translations = None
        yield
    finally:
        if hasattr(ctx, 'forced_babel_locale'):
            del ctx.forced_babel_locale

        for key, value in orig_attrs.items():
            setattr(ctx, key, value)


class Domain(object):
    """Localization domain. By default will use look for tranlations in Flask
    application directory and "messages" domain - all message catalogs should
    be called ``messages.mo``.
    """

    def __init__(self, translation_directories=None, domain='messages'):
        if isinstance(translation_directories, str):
            translation_directories = [translation_directories]
        self._translation_directories = translation_directories
        self.domain = domain
        self.cache = {}

    def __repr__(self):
        return '<Domain({!r}, {!r})>'.format(self._translation_directories, self.domain)

    @property
    def translation_directories(self):
        if self._translation_directories is not None:
            return self._translation_directories
        babel = flask.current_app.extensions['babel']
        return babel.translation_directories

    def as_default(self):
        """Set this domain as default for the current request"""
        ctx = _get_current_context()

        if ctx is None:
            raise RuntimeError("No request context")

        ctx.babel_domain = self

    def get_translations_cache(self, ctx):
        """Returns dictionary-like object for translation caching"""
        return self.cache

    def get_translations(self):
        ctx = _get_current_context()

        if ctx is None:
            return support.NullTranslations()

        cache = self.get_translations_cache(ctx)
        locale = get_locale()
        try:
            return cache[str(locale), self.domain]
        except KeyError:
            translations = support.Translations()

            for dirname in self.translation_directories:
                catalog = support.Translations.load(
                    dirname,
                    [locale],
                    self.domain
                )
                translations.merge(catalog)
                # FIXME: Workaround for merge() being really, really stupid. It
                # does not copy _info, plural(), or any other instance variables
                # populated by GNUTranslations. We probably want to stop using
                # `support.Translations.merge` entirely.
                if hasattr(catalog, 'plural'):
                    translations.plural = catalog.plural

            cache[str(locale), self.domain] = translations
            return translations

    def gettext(self, string, **variables):
        """Translates a string with the current locale and passes in the
        given keyword arguments as mapping to a string formatting string.

        ::

            gettext(u'Hello World!')
            gettext(u'Hello %(name)s!', name='World')
        """
        t = self.get_translations()
        s = t.ugettext(string)
        return s if not variables else s % variables


def _get_current_context():
    if flask.ctx.has_request_context():
        return flask.request

    if flask.current_app:
        return flask.current_app


def get_domain():
    ctx = _get_current_context()
    if ctx is None:
        # this will use NullTranslations
        return Domain()

    try:
        return ctx.babel_domain
    except AttributeError:
        pass

    babel = flask.current_app.extensions['babel']
    ctx.babel_domain = babel.domain_instance
    return ctx.babel_domain


# Create shortcuts for the default Flask domain
def gettext(*args, **kwargs):
    return get_domain().gettext(*args, **kwargs)
_ = gettext
